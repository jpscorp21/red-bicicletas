var Usuario = require('../../models/usuario');

exports.usuarios_list = function (req, res) {
  console.log('holaaa');
  Usuario.find({}, (err, usuarios) => {
    if (err) res.status(500).send();

    res.status(200).json({
      usuarios,
    });
  });
};

exports.usuarios_create = function (req, res) {
  console.log('creando');
  var usuario = new Usuario({
    nombre: req.body.nombre,
  });
  usuario.save(function (err) {
    res.status(201).json(usuario);
  });
};

exports.usuarios_reservar = function (req, res) {
  Usuario.findById(req.body.id, (err, usuario) => {
    console.log(usuario);
    usuario.reservar(
      req.body.bici_id,
      req.body.desde,
      req.body.hasta,
      (err) => {
        console.log('reserva');
        res.status(200).send();
      }
    );
  });
};
