var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
  Bicicleta.allBicis((err, bicis) => {
    if (err) res.status(500).send();

    res.status(200).json({
      bicicletas: bicis,
    });
  });
};

exports.bicicleta_create = function (req, res) {
  var bici = new Bicicleta({
    code: req.body.id,
    color: req.body.color,
    modelo: req.body.modelo,
  });
  bici.save();

  res.status(201).json({
    bicicleta: bici,
  });
};

exports.bicicleta_update = function (req, res) {
  Bicicleta.findOneAndUpdate(
    { code: req.params.id },
    {
      code: req.body.code,
      color: req.body.color,
      modelo: req.body.modelo,
    },
    (err, bici) => {
      if (err) res.status(500).send();
      bici.save();
      res.status(204).send();
    }
  );
  // Bicicleta.findByCode(req.params.id, (err, bici) => {
  //   if (err) res.status(500);

  // });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeByCode(req.params.id, (err, _) => {
    if (err) res.status(500).send();

    res.status(204).send();
  });
};
