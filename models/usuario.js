const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Reserva = require('./reserva');
const bcrypt = require('bcrypt');

const saltRound = 10;

const validateEmail = function (email) {
  return true;
};

const usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, 'El nombre es obligatorio'],
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es obligatorio'],
    lowercase: true,
    validate: [validateEmail, 'Por favor. Ingrese un email válido'],
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio'],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: String,
  verificado: {
    type: Boolean,
    default: false,
  },
});

usuarioSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, saltRound);
  }
  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
  const reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde,
    hasta,
  });
  console.log(reserva);
  reserva.save(cb);
};

module.exports = mongoose.model('Usuario', usuarioSchema);
