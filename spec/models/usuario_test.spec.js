var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Usuarios', () => {
  beforeEach((done) => {
    const mongoDB = 'mongodb://localhost/red_bicicleta_test';
    mongoose.createConnection(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    mongoose.Promise = global.Promise;
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error: '));
    db.once('open', () => {
      console.log('We are connected to test database!');
    });
    done();
  });

  afterEach((done) => {
    Reserva.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      Usuario.deleteMany({}, function (err, success) {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          done();
        });
      });
    });
  });
  // No tiene demasiado valor
  describe('Cuando un Usuario reserva una bici', () => {
    it('Desde existir la reserva', (done) => {
      const usuario = new Usuario({ nombre: 'Jean' });
      usuario.save();
      const bicicleta = new Bicicleta({
        code: 1,
        color: 'verde',
        modelo: 'urbana',
      });
      bicicleta.save();

      const hoy = new Date();
      const manhana = new Date();

      manhana.setDate(hoy.getDate() + 1);

      usuario.reservar(bicicleta.id, hoy, manhana, (err, reserva) => {
        Reserva.find({})
          .populate('bicicleta')
          .populate('usuario')
          .exec(function (err, reservas) {
            expect(reservas.length).toBe(1);
            expect(reservas[0].diasDeReserva()).toBe(2);
            expect(reservas[0].bicicleta.code).toBe(1);
            expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
            done();
          });
      });
    });
  });
});
