var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', () => {
  beforeEach((done) => {
    const mongoDB = 'mongodb://localhost/red_bicicleta_test';
    mongoose.createConnection(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    mongoose.Promise = global.Promise;
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error: '));
    db.once('open', () => {
      console.log('We are connected to test database!');
    });
    done();
  });

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });
  // No tiene demasiado valor
  describe('Bicicleta.createInstance', () => {
    it('Crear una instancia de bicicleta', (done) => {
      var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.5]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe('verde');
      expect(bici.modelo).toBe('urbana');
      expect(bici.ubicacion[0]).toBe(-34.5);
      expect(bici.ubicacion[1]).toBe(-54.5);
      done();
    });
  });
  describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        console.log('bicis', bicis);
        expect(bicis.length).toEqual(0);
        done();
      });
    });
  });
  describe('Bicicleta.add', () => {
    it('Agrega solo una bici', (done) => {
      var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
      Bicicleta.add(aBici, (err, newBici) => {
        if (err) console.log(err);
        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });
  describe('Bicicleta.findByCode', () => {
    it('Debe devolver la bici con code 1', (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({
          code: 1,
          color: 'verde',
          modelo: 'urbana',
        });

        Bicicleta.add(aBici, (err, newBici) => {
          if (err) console.log(err);

          var aBici2 = new Bicicleta({
            code: 2,
            color: 'rojo',
            modelo: 'urbana',
          });

          Bicicleta.add(aBici2, (err, newBici) => {
            if (err) console.log(err);

            Bicicleta.findByCode(1, function (error, targetBici) {
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });
  describe('Bicicleta.removeByCode', () => {
    it('Debe eliminar una bici con code 1', (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({
          code: 1,
          color: 'verde',
          modelo: 'urbana',
        });
        Bicicleta.add(aBici, (err, newBici) => {
          Bicicleta.removeByCode(1, (err, res) => {
            Bicicleta.allBicis(function (err, bicis) {
              expect(bicis.length).toBe(0);
              done();
            });
          });
        });
      });
    });
  });
});

// No se sabe en que orden se ejecuta
// beforeEach(() => {
//   Bicicleta.allBicis = [];
// });

// describe('Bicicleta.allBicis', () => {
//   it('Comienza vacia', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//   });
// });

// describe('Bicicleta.add', () => {
//   it('Agregando una', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);

//     var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
//     Bicicleta.add(a);

//     expect(Bicicleta.allBicis.length).toBe(1);
//     expect(Bicicleta.allBicis[0]).toBe(a);
//   });
// });

// describe('Bicicleta.findById', () => {
//   it('Debe devolver la bici con id 1', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);

//     var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
//     Bicicleta.add(a);

//     var targetBici = Bicicleta.findById(1);

//     expect(targetBici.id).toBe(1);
//     expect(targetBici.color).toBe(a.color);
//     expect(targetBici.modelo).toBe(a.modelo);
//   });
// });

// describe('Bicicleta.removeById', () => {
//   it('Debe eliminar una bici', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);

//     var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
//     Bicicleta.add(a);

//     Bicicleta.removeById(1);

//     expect(Bicicleta.allBicis.length).toBe(0);
//   });
// });
