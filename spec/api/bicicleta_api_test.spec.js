const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');
const mongoose = require('mongoose');

const base_url = 'http://localhost:3000/api/v1/bicicletas';

describe('Bicicleta API', () => {
  beforeEach((done) => {
    const mongoDB = 'mongodb://localhost/red_bicicleta_test';
    mongoose.createConnection(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error: '));
    db.once('open', () => {
      console.log('We are connected to test database!');
    });
    done();
  });

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });
  describe('GET BICICLETAS /', () => {
    it('Status 200', (done) => {
      request.get(base_url, (error, response, body) => {
        const result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });
  describe('POST BICICLETAS /create', () => {
    it('Status 200', (done) => {
      const headers = { 'content-type': 'application/json' };
      const aBici =
        '{"id": 10, "color": "negro", "modelo": "urbana", "lat": 34, "lng": -54}';

      request.post(
        {
          headers,
          url: `${base_url}/create`,
          body: aBici,
        },
        (error, response, body) => {
          expect(response.statusCode).toBe(201);
          Bicicleta.findByCode(10, (err, bici) => {
            if (err) console.log(err);
            expect(bici.color).toBe('negro');
            done();
          });
        }
      );
    });
  });
  describe('PUT BICICLETAS /update/:id', () => {
    it('Status 204', (done) => {
      var a = new Bicicleta({ code: 1, color: 'negro', modelo: 'urbana' });
      a.save();

      const headers = { 'content-type': 'application/json' };
      const aBici =
        '{"code": 1, "color": "negro update", "modelo": "urbana update"}';

      request.put(
        {
          headers,
          url: `${base_url}/update/1`,
          body: aBici,
        },
        (error, response, body) => {
          Bicicleta.findByCode(1, (err, bici) => {
            if (err) console.log(err);
            expect(response.statusCode).toBe(204);
            expect(bici.color).toBe('negro update');
            expect(bici.modelo).toBe('urbana update');
            done();
          });
        }
      );
    });
  });
  describe('DELETE BICICLETAS /delete/:id', () => {
    it('Status 204', (done) => {
      var a = new Bicicleta({ code: 1, color: 'negro', modelo: 'urbana' });
      a.save();

      const headers = { 'content-type': 'application/json' };

      request.delete(
        {
          headers,
          url: `${base_url}/delete/1`,
        },
        (error, response, body) => {
          Bicicleta.allBicis((err, bicis) => {
            expect(bicis.length).toBe(0);
            done();
          });
        }
      );
    });
  });
});
